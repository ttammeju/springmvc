package controller;

import dao.PostDao;
import model.Post;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
public class PostController {

	@Resource
	private PostDao daoPost;

	@GetMapping("posts")
	public List<Post> getPosts() {
		return daoPost.findAll();
	}

	@PostMapping("posts")
	public void addPost(@RequestBody @Valid Post post) {
		daoPost.save(post);
	}

	@DeleteMapping("posts/{id}")
	public void removePost(@PathVariable Long id) {
		daoPost.delete(id);
	}
}